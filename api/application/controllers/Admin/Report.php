<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Admin/m_report', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        if ($this->input->post('from') == '' && $this->input->post('to') == '') {
            $month      = date("F", mktime(0, 0, 0, $this->input->post('month'), 10));
            $timestamp  = strtotime($month.' '.date('Y'));
            $from       = date('01-m-Y', $timestamp);
            $to         = date('t-m-Y', $timestamp);
            $data['totalWeekday']   = $this->mainmodul->weekday($from, $to);
            $data['totalReport']    = $this->mainmodul->totalReport($this->input->post('id'),$this->input->post('month'),$this->input->post('year'),$this->input->post('from'),$this->input->post('to'));
        } else {
            $data['totalWeekday']   = $this->mainmodul->weekday($this->input->post('from'), $this->input->post('to'));
            $data['totalReport']    = $this->mainmodul->totalReport($this->input->post('id'),$this->input->post('month'),$this->input->post('year'),$this->input->post('from'),$this->input->post('to'));
        }
        
        $details = $this->mainmodul->get_details($this->input->post('id'),$this->input->post('month'),$this->input->post('year'),$this->input->post('from'),$this->input->post('to'));

        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
            'details'   => $details,
        ], 200);
    }
}

