<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Admin/m_dashboard', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data    = $this->mainmodul->get_data($this->input->post('agentID'));

        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
        ], 200);
    }
}

