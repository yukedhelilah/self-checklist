<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Admin/m_company', 'mainmodul');
        $this->load->model('Admin/m_staff', 'staff');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }
    
    public function get_data_post(){
        $data   = $this->mainmodul->get_data();

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $data,
        ], 200);                
    }
    
    public function save_data_post(){
        if ($this->input->post('act') == 'add') {
            $data   = array(
                'agentUsername'   => $this->input->post('agentUsername', true),
            );
            $sql    = $this->mainmodul->add_data($data);

            $data2   = array(
                'agentID'   => $sql,
                'name'      => strtoupper($this->input->post('staffName', true)),
                'username'  => $this->input->post('staffUsername', true),
                'password'  => md5('123456'),
                'level'     => 0,
            );
            $sql2    = $this->staff->add_data($data2);
        } else {
            $data = array(
                'agentUsername'  => $this->input->post('agentUsername', true),
            );
            $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));
        }

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
    
    public function delete_data_post(){
        $sql   = $this->mainmodul->delete_data($this->input->post('id'));

        if ($sql == true) {
            $data = array(
                'flag'      => 1,
            );
            $result   = $this->mainmodul->delete_staff($data, $this->input->post('id'));
        } else {
            $result = false;
        }

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $result,
        ], 200);                
    }
}

