<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Admin/m_staff', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }
    
    public function get_data_post(){
        $data   = $this->mainmodul->get_data($this->input->post('agentID'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $data,
        ], 200);                
    }
    
    public function save_data_post(){
        if ($this->input->post('act') == 'add') {
            $data   = array(
                'agentID'   => $this->input->post('agentID', true),
                'name'      => strtoupper($this->input->post('staffName', true)),
                'username'  => $this->input->post('staffUsername', true),
                'password'  => md5('123456'),
                'level'         => $this->input->post('level'),
            );
            $sql    = $this->mainmodul->add_data($data);
        } else {
            if(!empty($this->input->post('resetPassword'))){
                $data = array(
                    'name'      => strtoupper($this->input->post('staffName', true)),
                    'username'  => $this->input->post('staffUsername', true),
                    'password'  => md5('123456'),
                    'level'     => $this->input->post('level'),
                );
            } else {
                $data = array(
                    'name'      => strtoupper($this->input->post('staffName', true)),
                    'username'  => $this->input->post('staffUsername', true),
                    'level'     => $this->input->post('level'),
                );
            }
            $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));
        }

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
    
    public function delete_data_post(){
        $data = array(
            'flag'      => 1,
        );
        $sql   = $this->mainmodul->edit_data($data,$this->input->post('id'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
}

