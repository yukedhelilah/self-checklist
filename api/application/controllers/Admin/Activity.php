<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends App_Public {

    public function __construct(){
        parent::__construct();
        
        $this->load->model('Admin/m_activity', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data    = $this->mainmodul->get_data($this->input->post('id'), $this->input->post('agentID'),$this->input->post('month'),$this->input->post('year'),$this->input->post('from'),$this->input->post('to'));

        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
        ], 200);
    }
}

