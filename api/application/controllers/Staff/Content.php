<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Staff/m_content', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function upload_post(){
        $uploadpath = realpath(APPPATH . 'files/upload/');
        $status = "";
        $msg    = "";
        $file_element_name = 'userfile';

        if ($status != "error")
        {
            $config['upload_path']      = 'files/upload/';
            $config['allowed_types']    = 'gif|jpg|png';
            $config['max_size']         = 1024 * 8;

            $new_name                   = 'img_'.time().'_'.$_POST['no'];
            $config['file_name']        = $new_name;
            //$config['encrypt_name']     = TRUE;
      
            $this->load->library('upload', $config);
            // $this->upload->initialize($config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg.'*'.$_POST['no'];
    }

    public function post_content_post(){
        $data = array(
            'caption'       => $_POST['caption'],
            'img1'          => $_POST['image1'],
            'img2'          => $_POST['image2'],
            'img3'          => $_POST['image3'],
            'location'      => $_POST['city'].', '.$_POST['region'].', '.$_POST['country'],
            'latitude'      => $_POST['latitude'],
            'longitude'     => $_POST['longitude'],
            'uploadBy'      => $_POST['staffID'],
            'uploadDate'    => date('Y-m-d H:i:s'),
        );
        $sql    = $this->mainmodul->post_content($data);

        if ($sql == true) {
            $this->response([
                'code'      => 200,
                'message'   => 'Success',
                'data'      => $sql
            ], 200); 
        } else {
            $this->response([
                'code'      => 500,
                'message'   => 'Failed',
                'error'     => $this->db->error(),
            ], 200);
        }
    }

    public function delete_content_post(){
        $sql    = $this->mainmodul->delete_content($this->input->post('id'));
        
        if ($sql == true) {
            $this->response([
                'code'      => 200,
                'message'   => 'Success',
                'data'      => $sql
            ], 200); 
        } else {
            $this->response([
                'code'      => 500,
                'message'   => 'Failed',
                'error'     => $this->db->error(),
            ], 200);
        }
    }
    
    public function get_all_content_post(){
        $sql    = $this->mainmodul->get_all_content($this->input->post('start'),$this->input->post('limit'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql
        ], 200); 
    }
    
    public function get_my_content_post(){
        $sql    = $this->mainmodul->get_my_content($this->input->post('staffID'),$this->input->post('start'),$this->input->post('limit'));

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql
        ], 200); 
    }
}