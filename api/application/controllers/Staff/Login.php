<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Staff/m_login', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }
    
    public function login_post(){
        // echo $this->input->post('username');
        // echo md5($this->input->post('password'));
        // die();
        $required = [];
        if(empty($this->input->post('username'))){
            $required[] = 'Username';
        }

        if(empty($this->input->post('password'))){
            $required[] = 'Password';
        }

        if(count($required)>0){
            $error = [
                'reason'    => 'required',
                'message'   => join(", ",$required).' is required',
            ];
            $this->response([
                'code'      => 401,
                'message'   => 'Unauthorized',
                'errors'    => $error,
            ], 200);
        }

        $data = $this->mainmodul->login($this->input->post('username'), md5($this->input->post('password')));

        if (isset($data['error'])) {
            $error = [
               'message'   => $data['error']
            ];
            $this->response([
                'code'      => 500,
                'message'   => 'Internal Server Error',
                'errors'    => $error,
            ], 200);
        }else if(count($data) == 0){
            $error = [
               'message'   => 'Data not found'
            ];
            $this->response([
                'code'      => 500,
                'message'   => 'Internal Server Error',
                'errors'    => $error,
            ], 200);
        }else{
            $this->response([
                'code'      => 200,
                'message'   => 'Success',
                'data'      => $data,
            ], 200);     

        }
    }

    public function change_password_post(){
        $data=array( 
            'password'  => md5($this->input->post('password'))
        );

        $sql   = $this->mainmodul->change_password($this->input->post('staffID'),$data);

        $this->response([
            'code'      => 200,
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
}

