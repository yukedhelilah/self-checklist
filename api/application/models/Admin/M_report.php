<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_report extends CI_Model
{   
    function totalReport($id,$month,$year,$from,$to){
        $this->db->select('a.uploadDate, COUNT(a.id) as total');
        $this->db->from('content a');
        $this->db->join('ms_user b','a.uploadBy=b.id','left');
        $this->db->where('b.id', $id);
        $this->db->group_by('DATE(a.uploadDate)');

        if ($from == '' && $to == '') {
            $this->db->where('MONTH(a.uploadDate)', $month);
            $this->db->where('YEAR(a.uploadDate)', $year);
        } 

        if ($from != '') {
            $this->db->where('a.uploadDate >=', date('Y-m-d', strtotime($from)));
        }

        if ($to != '') {
            $this->db->where('a.uploadDate <=', date('Y-m-d', strtotime($to)));
        }

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function weekday($from, $to) {
        $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
        $holidayDays = ['*-12-25', '*-01-01', '2013-12-23']; # variable and fixed holidays

        $from = new DateTime($from);
        $to = new DateTime($to);
        $to->modify('+1 day');
        $interval = new DateInterval('P1D');
        $periods = new DatePeriod($from, $interval, $to);

        $days = 0;
        foreach ($periods as $period) {
            if (!in_array($period->format('N'), $workingDays)) continue;
            if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
            if (in_array($period->format('*-m-d'), $holidayDays)) continue;
            $days++;
        }
        return $days;
    }

    function get_details($id,$month,$year,$from,$to){
        $select = array(
            'a.*',
            'b.name'
        );

        $this->db->select($select);
        $this->db->from('content a');
        $this->db->join('ms_user b','a.uploadBy=b.id');
        $this->db->where('b.id', $id);
        $this->db->order_by('uploadDate', 'desc');

        if ($from == '' && $to == '') {
            $this->db->where('MONTH(a.uploadDate)', $month);
            $this->db->where('YEAR(a.uploadDate)', $year);
        } 

        if ($from != '') {
            $this->db->where('a.uploadDate >=', date('Y-m-d', strtotime($from)));
        }

        if ($to != '') {
            $this->db->where('a.uploadDate <=', date('Y-m-d', strtotime($to)));
        }

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }
}