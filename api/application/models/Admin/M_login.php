<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_login extends CI_Model
{   

    function login($agentID, $username, $password){
        $this->db->select('a.*, b.agentUsername as agentName');
        $this->db->from('ms_user a');
        $this->db->from('ms_agent b','a.agentID=b.id');
        $this->db->where('a.level', '0');
        $this->db->where('a.flag', '0');
        $this->db->where('b.agentUsername', $agentID);
        $this->db->where('a.username', $username);
        $this->db->where('a.password', $password);

        $query = $this->db->get();

        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $password = $row['password'] == md5('123456') ? '0' : '1';

            $tokenKey = $row['username'] . '|' . date('Y-m-d H:i:s');
            return [
                'id'            => $row['id'],
                'agentID'       => $row['agentID'],
                'agentName'     => $row['agentName'],
                'name'          => $row['name'],
                'username'      => $row['username'],
                'password'      => $password,
                'token'         => md5($tokenKey),
                'expired'       => date("Y-m-d"),
            ];
        } else {
            return NULL;
        }
        
        return []; 
    }  

    function change_password($id,$data){
        $this->db->where('id',$id);
        $this->db->update('ms_user', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}