<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_staff extends CI_Model
{   
    function get_data($agentID){
        $this->db->select('id,username,password,level,flag,CASE WHEN level = 1 THEN name ELSE CONCAT(name, " (Admin)") END AS name');
        $this->db->from('ms_user');
        // $this->db->where('level', 1);
        $this->db->where('flag', 0);
        $this->db->where('agentID', $agentID);
        $this->db->order_by('name', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function add_data($data){
        $this->db->insert('ms_user', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function edit_data($data,$id){
        $this->db->where('id',$id);
        $this->db->update('ms_user', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}