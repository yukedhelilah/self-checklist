<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_activity extends CI_Model
{   
    function get_data($id, $agentID,$month,$year,$from,$to){
        $select = array(
            'a.*',
            'b.name'
        );

        $this->db->select($select);
        $this->db->from('content a');
        $this->db->join('ms_user b','a.uploadBy=b.id');
        $this->db->where('agentID', $agentID);
        $this->db->order_by('uploadDate', 'desc');

        if ($id > 0) {
            $this->db->where('a.uploadBy', $id);
        }

        if ($from == '' && $to == '') {
            $this->db->where('MONTH(a.uploadDate)', $month);
            $this->db->where('YEAR(a.uploadDate)', $year);
        } 

        if ($from != '') {
            $this->db->where('a.uploadDate >=', date('Y-m-d', strtotime($from)));
        }

        if ($to != '') {
            $this->db->where('a.uploadDate <=', date('Y-m-d', strtotime($to)));
        }

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }
}