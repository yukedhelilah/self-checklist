<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_company extends CI_Model
{   
    function get_data(){
        $this->db->select('*');
        $this->db->from('ms_agent');
        $this->db->order_by('agentUsername', 'asc');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function add_data($data){
        $this->db->insert('ms_agent', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function edit_data($data,$id){
        $this->db->where('id',$id);
        $this->db->update('ms_agent', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_data($id){
        $this->db->where('id',$id);
        $this->db->delete('ms_agent');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete_staff($data,$id){
        $this->db->where('agentID',$id);
        $this->db->update('ms_user', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}