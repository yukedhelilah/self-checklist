<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_dashboard extends CI_Model
{   
    function get_data($agentID){
        $select = array(
            'COUNT(a.id) as total',
            'b.name'
        );

        $this->db->select('CASE WHEN level = 1 THEN name ELSE CONCAT(name, " (Admin)") END AS name');
        $this->db->select('(SELECT COUNT(aa.id) FROM content aa WHERE aa.uploadBy=a.id AND DATE(aa.uploadDate) = CURDATE()) total', true);
        $this->db->from('ms_user a');
        $this->db->join('content b','a.id=b.uploadBy','left');
        $this->db->where('agentID', $agentID);
        // $this->db->where('level', 1);
        $this->db->where('flag', 0);
        $this->db->order_by('a.name', 'asc');
        $this->db->group_by('a.id');

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }
}