<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_content extends CI_Model
{   
    function post_content($data){
        $this->db->insert('content', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return ['error' => $this->db->error()];
        }
    }

    function delete_content($id){
        $this->db->where('id', $id);  
        return $this->db->delete('content');
    }

    function get_all_content($start, $limit){
        $select = array(
            'a.id',
            'a.caption',
            'a.img1',
            'a.img2',
            'a.img3',
            'a.location',
            'b.name',
            'a.uploadBy',
            'a.uploadDate'
        );

        $this->db->select($select);
        $this->db->from('content a');
        $this->db->join('ms_user b','a.uploadBy=b.id');
        $this->db->where('b.flag', 0);
        $this->db->order_by('a.uploadDate', 'desc');
        $this->db->limit($limit, $start);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_my_content($staffID, $start, $limit){
        $select = array(
            'a.id',
            'a.caption',
            'a.img1',
            'a.img2',
            'a.img3',
            'a.location',
            'b.name',
            'a.uploadBy',
            'a.uploadDate'
        );

        $this->db->select($select);
        $this->db->from('content a');
        $this->db->join('ms_user b','a.uploadBy=b.id');
        $this->db->where('b.flag', 0);
        $this->db->where('b.id', $staffID);
        $this->db->order_by('a.uploadDate', 'desc');
        $this->db->limit($limit, $start);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }
}