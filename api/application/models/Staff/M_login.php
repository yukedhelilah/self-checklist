<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class M_login extends CI_Model
{   

    function login($username, $password){
        $this->db->select('*');
        $this->db->from('ms_user');
        $this->db->where('flag', '0');
        $this->db->where('level', '1');
        $this->db->where('username', $username);
        $this->db->where('password', $password);

        $query = $this->db->get();

        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $password = $row['password'] == md5('123456') ? '0' : '1';

            $tokenKey = $row['name'] . '|' . date('Y-m-d H:i:s');
            return [
                'id'            => $row['id'],
                'username'      => $row['username'],
                'name'          => $row['name'],
                'password'      => $password,
                'token'         => md5($tokenKey),
                'expired'       => date("Y-m-d"),
            ];
        } else {
            return NULL;
        }
        
        return []; 
    }  

    function change_password($id,$data){
        $this->db->where('id',$id);
        $this->db->update('ms_user', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}