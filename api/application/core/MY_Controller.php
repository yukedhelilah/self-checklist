<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Application extends CI_Controller {

    public function __construct() {
        parent::__construct();

        /**/
    }

}

class App_Public extends REST_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        /**/
    }
    
}

class App_Private extends REST_Controller {

    public function __construct() {
        parent::__construct();

        if (!isAuth()) {
            $this->response(array('status' => false), 401);
        }
    }

}
