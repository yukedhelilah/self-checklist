var format = 'dd-mm-yy',
from = $('#dateFrom')
.datepicker({
  dateFormat: 'dd-mm-yy',
  defaultDate: '+1w',
  numberOfMonths: 1
})
.on('change', function() {
  to.datepicker('option','minDate', getDate( this ) );
}),
to = $('#dateTo').datepicker({
  dateFormat: 'dd-mm-yy',
  defaultDate: '+1w',
  numberOfMonths: 1
})
.on('change', function() {
  from.datepicker('option','maxDate', getDate( this ) );
});

function getDate( element ) {
  var date;
  try {
    date = $.datepicker.parseDate( format, element.value );
  } catch( error ) {
    date = null;
  }

  return date;
}

$(function(){
  'use strict'

  var now   = new Date();
  var month = (now.getMonth()+1) < 10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1);

  $('#month').val(month);

  $('#optionDate').on('change',function(){
    if ($(this).val() == 0) {
      $('.monthly').css('display','block');
      $('.dateRange').css('display','none');
      $('#dateFrom').val('');
      $('#resetDateFrom').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
      $('#dateTo').val('');
      $('#resetDateTo').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    } else {
      $('.monthly').css('display','none');
      $('.dateRange').css('display','block');
      $('#dateFrom').val('');
      $('#resetDateFrom').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
      $('#dateTo').val('');
      $('#resetDateTo').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    }
  });

  $('#dateFrom, #dateTo, #month, #year').on('change', function() {
    get_data($('#staffID').val());
  }); 

  $('#dateFrom').on('change keyup paste', function (e) {
    if ($('#dateFrom').val() != '') {
      $('#resetDateFrom').attr('style','color:white;background-color:#dc3545;border:1px solid #dc3545');
    } else {
      $('#resetDateFrom').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    }
  });

  $('#resetDateFrom').click(function() {
    $('#dateFrom').val('');
    $('#resetDateFrom').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    get_data($('#staffID').val());
  });

  $('#dateTo').on('change keyup paste', function (e) {
    if ($('#dateTo').val() != '') {
      $('#resetDateTo').attr('style','color:white;background-color:#dc3545;border:1px solid #dc3545');
    } else {
      $('#resetDateTo').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    }
  });

  $('#resetDateTo').click(function() {
    $('#dateTo').val('');
    $('#resetDateTo').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    get_data($('#staffID').val());
  });

  get_staff();
  $('#staffID').on('change',function(){
    get_data($(this).val());
  });
})

function get_staff(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'http://creatrixorganizer.com/sc/api/Admin/staff/get_data/_',
    // url: 'http://localhost/asiaholiday/api/admin/staff/get_data/_',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: {
      agentID : agentID,
    },
    success: function(responses) {
      $('#staffID').empty();
      if(responses.code == 200){
        $.each(responses.data, function(i, val){
          if (i == 0) {
            $('#staffID').append(`
              <option value="`+val.id+`">`+val.name+`</option>
            `);
            get_data(val.id);
          } else {
            $('#staffID').append(`
              <option value="`+val.id+`">`+val.name+`</option>
            `);
          }
        });
      }
    }
  });
}

function get_data(id){
  $('#totalWeekday, #totalReport, #totalChecklist').html('');
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'http://creatrixorganizer.com/sc/api/Admin/report/get_data/_',
    // url: 'http://localhost/asiaholiday/api/admin/report/get_data/_',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data :{
      id    : id,
      month : $('#month').val(),
      year  : $('#year').val(),
      from  : $('#dateFrom').val(),
      to    : $('#dateTo').val(),
    },
    success: function(responses) {
      if(responses.code == 200){
        $('#totalWeekday').html(responses.data.totalWeekday)
        if (responses.data.totalReport != null) {
          var totalChecklist = 0;
          $.each(responses.data.totalReport, function( i, val ) {
            totalChecklist += parseInt(val.total);
          });
          console.log(totalChecklist)
          $('#totalReport').html(responses.data.totalReport.length);
          $('#totalChecklist').html(totalChecklist);
        } else {
          $('#totalReport').html('0');
          $('#totalChecklist').html('0');
        }

        var aaData = [];
        $.each(responses.details, function( i, value ) {
          var stringData  = value._;
          var no          = i + 1;
          var lokasi      = value.location.split(',');

          aaData.push([
            `<div style="text-align:center">` + no + `</div>`,
            dateFormat(value.uploadDate),
            hourFormat(value.uploadDate),
            lokasi[0]+`, `+lokasi[1],
            value.caption,
          ]);
        });
        $('#main-table').DataTable().destroy();
        $('#main-table').DataTable({
        // 'responsive': true,
          'language'  : {
            'searchPlaceholder'   : 'Search...',
            'sSearch'       : '',
            'lengthMenu'      : '_MENU_ items/page',
            },
          'aaData'  : aaData,
          "bJQueryUI" : true,
          "aoColumns" : [
            { "sWidth"  : "1%" },  
            { "sWidth"  : "5%" }, 
            { "sWidth"  : "1%" },  
            { "sWidth"  : "10%" }, 
            { "sWidth"  : "8%" }, 
          ],
          'aLengthMenu' : [
            [5, 10, 50, -1],
            [5, 10, 50, "All"]
          ],
          'dom' : 'Bfrtip',
          'buttons': [ {
              'extend'    : 'excelHtml5',
              'autoFilter': true,
              'sheetName' : 'Exported data'
          }, 'copy', 'print' ]
        });
      }
    }
  });
}