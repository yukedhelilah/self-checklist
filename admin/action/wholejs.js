var agentID     = atob(localStorage.getItem(btoa("admin_agentID")));
var agentName   = atob(localStorage.getItem(btoa("admin_agentName")));
var adminID     = atob(localStorage.getItem(btoa("admin_id")));
var name     	  = atob(localStorage.getItem(btoa("admin_name")));
var username    = atob(localStorage.getItem(btoa("admin_username")));
var password    = atob(localStorage.getItem(btoa("admin_password")));
var token    	  = atob(localStorage.getItem(btoa("admin_token")));
var expired     = localStorage.getItem(btoa("admin_expired"));
var exp         = new Date(atob(expired));
var now         = new Date();

if(agentID == null || name == null || username == null || token == null || expired == null || exp.getDate() +`-`+ exp.getMonth() +`-`+ exp.getFullYear() != now.getDate() +`-`+ now.getMonth() +`-`+ now.getFullYear()){
    removeLocalstorage();
    window.location.href = '../pages/login';
}

if (password == 0) {
    $('#modal_change_password').modal({backdrop: 'static', keyboard: false});
    $('#adminID').val(adminID);
}

$(document).ready(function() {
    $('#loginName').html(name);
    $('.df-logo').html(agentName.toUpperCase());

    if (agentID == 1) {
      $('#nav_company').css('display','block');
    } else {
      $('#nav_company').css('display','none');
    }
});

var monthNames  = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
var weekday     = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];

function dateFormat(d) {
  var t       = new Date(d);
  var now     = new Date();

  return weekday[t.getDay()] + ', ' +t.getDate() + ' ' + monthNames[t.getMonth()] + ' ' + t.getFullYear();
}

function hourFormat(d) {
  var t       = new Date(d);
  var hours   = t.getHours() < 10 ? '0'+t.getHours() : t.getHours();
  var minutes = t.getMinutes() < 10 ? '0'+t.getMinutes() : t.getMinutes(); 

  return hours + ':' + minutes;
}

function changePassword(){
    if($('#password').val() == ''){
        alert('Password must be required');
    } else if ($('#password').val().length != 6) {
        alert('Password must be 4 digits');
    } else if ($('#password').val() == '123456'){
        alert('Password must not be 123456');
    } else {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'http://creatrixorganizer.com/sc/api/Admin/login/change_password/_',
            // url: 'http://localhost/asiaholiday/api/Admin/login/change_password/_',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: {
                token           : '1b787b70ed2e0de697d731f14b5da57b',
                id              : $('#adminID').val(),
                password        : $('#password').val()
            },
            success: function(response) {
                alert('success');
                removeLocalstorage();
                window.location.href = '../pages/login';
            },
        });
    }
}

function logout() {
    if (confirm("Do you want to logout?")) {
        removeLocalstorage();
        window.location.href = '../pages/login';
    } 
}

function removeLocalstorage(){
    localStorage.removeItem(btoa('admin_id'));
    localStorage.removeItem(btoa('admin_name'));
    localStorage.removeItem(btoa('admin_username'));
    localStorage.removeItem(btoa('admin_password'));
    localStorage.removeItem(btoa('admin_token'));
    localStorage.removeItem(btoa('admin_expired'));
}

function getUrlParameter(sParam){
  var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
  }
}

function numberFormat(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}