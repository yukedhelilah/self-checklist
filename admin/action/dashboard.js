$(function(){
  'use strict'

  get_data();
})

function get_data(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'http://creatrixorganizer.com/sc/api/Admin/dashboard/get_data/_',
    // url: 'http://localhost/asiaholiday/api/admin/dashboard/get_data/_',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: {
      agentID : agentID,
    },
    success: function(responses) {
      if(responses.code == 200){
        $.each(responses.data, function( i, val ) {
          $('#konten').append(`
            <div class="col-sm-6 col-lg-4 mg-t-10 mg-lg-t-0 mg-b-10">
              <div class="card card-body">
                <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-8" style="text-align: center;">`+val.name+`</h6>
                <div class="d-flex d-lg-block d-xl-flex align-items-end">
                  <table class="table table-bordered mg-b-0 tabelGift">
                    <tr>
                      <td colspan="3" style="text-align: center;height: 45px">
                        <p class="mg-0" style="font-size: 45px;font-weight: bold;color: #dc3545">`+val.total+`</p>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          `);
        });
      }
    }
  });
}