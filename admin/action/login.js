$('input[name="agentID"]').focus();

$(document).ready(function() { 
    if (localStorage.getItem(btoa('admin_agentID')) != null && localStorage.getItem(btoa('admin_expired')) != null && localStorage.getItem(btoa('admin_token')) != null && localStorage.getItem(btoa('admin_username')) != null && localStorage.getItem(btoa('admin_name')) != null) {
        window.location.href = '../pages/dashboard';
    }
    
    $('#password').keypress(function(e) {
        if(e.which == 13) {
            login($("#agentID").val(),$("#username").val(),$("#password").val());
        }
    });

    $('#login').click(function() {
        login($("#agentID").val(),$("#username").val(),$("#password").val());
    });
});

function login(agentID, username, password){  
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://creatrixorganizer.com/sc/api/Admin/login/login/_',
        // url: 'http://localhost/asiaholiday/api/Admin/login/login/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            agentID     : agentID,
            username    : username,
            password    : password,
        },
        success: function(response) {
            if (response.code == '200') {
                localStorage.setItem(btoa('admin_id'), btoa(response.data.id));
                localStorage.setItem(btoa('admin_agentID'), btoa(response.data.agentID));
                localStorage.setItem(btoa('admin_agentName'), btoa(response.data.agentName));
                localStorage.setItem(btoa('admin_name'), btoa(response.data.name));
                localStorage.setItem(btoa('admin_username'), btoa(response.data.username));
                localStorage.setItem(btoa('admin_password'), btoa(response.data.password));
                localStorage.setItem(btoa('admin_token'), btoa(response.data.token));
                localStorage.setItem(btoa('admin_expired'), btoa(response.data.expired));
                window.location.href = '../pages/dashboard';
            }else{
                alert("Your data is not verified");
            }
        },
    });
}