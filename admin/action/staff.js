$(document).ready(function() {
	get_data();
});

function get_data(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://creatrixorganizer.com/sc/api/Admin/staff/get_data/_',
        // url: 'http://localhost/asiaholiday/api/Admin/staff/get_data/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            agentID     : agentID,
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;

                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    `<span style="text-align:center">` + value.name + `</span>`, 
                    value.username,
                    `<div style="text-align:center">
                    	<div data-toggle="tooltip" title="Edit" onclick="edit_data('`+value.id+`','`+value.name+`','`+value.username+`','`+value.level+`')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
                		</div>
                    	<div data-toggle="tooltip" title="Delete" onclick="delete_data(`+value.id+`)" class="btn btn-xs btn-danger" style="padding: 1px 5px;">
                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-trash"></i>
                		</div>
            		</div>`,
                ]);
            });
            $('#tb_admin').DataTable().destroy();
        	$('#tb_admin').DataTable({
				'responsive': true,
				'language'	: {
					'searchPlaceholder'		: 'Search...',
					'sSearch'				: '',
					'lengthMenu'			: '_MENU_ items/page',
			  	},
				'aaData'	: aaData,
				"bJQueryUI"	: true,
				"aoColumns"	: [
					{ "sWidth"	: "5%" },  
					{ "sWidth"	: "44%" }, 
					{ "sWidth"	: "44%" }, 
					{ "sWidth"	: "7%" },
				],
				'aLengthMenu'	: [
					[10, 20, 50, -1],
					[10, 20, 50, "All"]
				],
			});
        },
    });
}

function add_data(){
	$('#modal_add').modal('show');
	$('#modal_tittle').html('Add New Data');
	$('#edit_password').css('display','none');
	$('#act').val('add');
	$('#id').val('');
	$('#staffName').val('');
	$('#staffUsername').val('');
	$('#staffUsername').prop('readonly',false);
	$('#staffPassword').val('123456');
	$('#groupLevel').css('display','block');
}

function edit_data(id,name,username,level){
	$('#modal_add').modal('show');
	$('#modal_tittle').html('Edit Data');
	$('#edit_password').css('display','block');
	$('#act').val('edit');
	$('#id').val(id);
	$('#staffName').val(name.replace(" (Admin)", ""));
	$('#staffUsername').val(username);
	$('#staffUsername').prop('readonly',true);
	$('#staffPassword').val('******');
	$('#resetPassword').prop('checked',false);
	$('#level'+level).attr('checked', true);
	$('#groupLevel').css('display','none');
}

function delete_data(id){
	$('#modal_delete').modal('show');
    $('#delete_data').click(function() {
	    $.ajax({
	        type: 'POST',
	        dataType: 'JSON',
        	url: 'http://creatrixorganizer.com/sc/api/Admin/staff/delete_data/_',
	        // url: 'http://localhost/asiaholiday/api/Admin/staff/delete_data/_',
	        headers: {
	            'Content-Type': 'application/x-www-form-urlencoded'
	        },
	        data: {
	            token	: '1b787b70ed2e0de697d731f14b5da57b',
	            id		: id
	        },
	        success: function(response) {
        		if (response.code == 200) {
        			alert('success');
					$('#modal_delete').modal('hide');
		        	get_data();
		        } else {
        			alert('failed, try again later');
					$('#modal_delete').modal('hide');
		        	get_data();
		        }
	        },
	    });
    });
}

function save(){
	var level = "";
	if ($('#level0').is(':checked')) {
		level = '0';
	} else{
		level = '1';
	}
	if ($('#staffName').val() == '' || $('#staffUsername').val() == '') {
		alert('Data must be required!');
	} else {
	    $.ajax({
	        type: 'POST',
	        dataType: 'JSON',
        	url: 'http://creatrixorganizer.com/sc/api/Admin/staff/save_data/_',
	        // url: 'http://localhost/asiaholiday/api/Admin/staff/save_data/_',
	        headers: {
	            'Content-Type': 'application/x-www-form-urlencoded'
	        },
	        data: $('#form_agent').serialize()+"&agentID="+agentID+"&level="+level,
	        success: function(response) {
	        	if (response.code == 200) {
	        		alert('success');
					$('#modal_add').modal('hide');
		        	get_data();
	        	} else {
	        		alert('failed, try again later');
					$('#modal_add').modal('hide');
		        	get_data();
	        	}
	        },
	    });
	}
}