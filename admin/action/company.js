$(document).ready(function() {
	get_data();
});

function get_data(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://creatrixorganizer.com/sc/api/Admin/company/get_data/_',
        // url: 'http://localhost/asiaholiday/api/Admin/company/get_data/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            agentID     : agentID,
        },
        success: function(response) {
            var aaData = [];
            $.each(response.data, function( i, value ) {
                var stringData  = value._;
                var no          = i + 1;
                var button		= '';

                if (value.id == 1) {
                	button = `<div style="text-align:center">
                    	<div data-toggle="tooltip" title="Edit" onclick="edit_data('`+value.id+`','`+value.agentUsername+`')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
                		</div>
            		</div>`;
                } else {
                	button = `<div style="text-align:center">
                    	<div data-toggle="tooltip" title="Edit" onclick="edit_data('`+value.id+`','`+value.agentUsername+`')" class="btn btn-xs btn-primary" style="padding: 1px 5px;">
                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-edit"></i>
                		</div>
                    	<div data-toggle="tooltip" title="Delete" onclick="delete_data(`+value.id+`)" class="btn btn-xs btn-danger" style="padding: 1px 5px;">
                    		<i style="vertical-align: middle;font-size: 10px;" class="fa fa-trash"></i>
                		</div>
            		</div>`;
                }

                aaData.push([
                    `<div style="text-align:center">` + no + `</div>`,
                    `<span style="text-align:center">` + value.agentUsername + `</span>`, 
                    button,
                ]);
            });
            $('#tb_admin').DataTable().destroy();
        	$('#tb_admin').DataTable({
				'responsive': true,
				'language'	: {
					'searchPlaceholder'		: 'Search...',
					'sSearch'				: '',
					'lengthMenu'			: '_MENU_ items/page',
			  	},
				'aaData'	: aaData,
				"bJQueryUI"	: true,
				"aoColumns"	: [
					{ "sWidth"	: "5%" },  
					{ "sWidth"	: "85%" }, 
					{ "sWidth"	: "10%" },
				],
				'aLengthMenu'	: [
					[10, 20, 50, -1],
					[10, 20, 50, "All"]
				],
			});
        },
    });
}

function add_data(){
	$('#modal_add').modal('show');
	$('#modal_tittle').html('Add New Data');
	$('#edit_password').css('display','none');
	$('#act').val('add');
	$('#id').val('');
	$('#agentUsername').val('');
	$('#staffName').val('');
	$('#staffUsername').val('');
	$('#staffUsername').prop('readonly',false);
	$('#staffPassword').val('123456');
	$('.add_company').css('display','block');
}

function edit_data(id,agentUsername){
	$('#modal_add').modal('show');
	$('#modal_tittle').html('Edit Data');
	$('#edit_password').css('display','block');
	$('#act').val('edit');
	$('#id').val(id);
	$('#agentUsername').val(agentUsername);
	$('.add_company').css('display','none');
}

function delete_data(id){
	$('#modal_delete').modal('show');
    $('#delete_data').click(function() {
	    $.ajax({
	        type: 'POST',
	        dataType: 'JSON',
        	url: 'http://creatrixorganizer.com/sc/api/Admin/company/delete_data/_',
	        // url: 'http://localhost/asiaholiday/api/Admin/company/delete_data/_',
	        headers: {
	            'Content-Type': 'application/x-www-form-urlencoded'
	        },
	        data: {
	            token	: '1b787b70ed2e0de697d731f14b5da57b',
	            id		: id
	        },
	        success: function(response) {
        		if (response.code == 200) {
        			alert('success');
					$('#modal_delete').modal('hide');
		        	get_data();
		        } else {
        			alert('failed, try again later');
					$('#modal_delete').modal('hide');
		        	get_data();
		        }
	        },
	    });
    });
}

function save(){
	var txt = '';
	if ($('#act').val() == 'add') {
		txt = $('#agentUsername').val() == '' || $('#staffName').val() == '' || $('#staffUsername').val() == ''; 
	} else {
		txt = $('#agentUsername').val() == '';
	}

	if (txt) {
		alert('Data must be required!');
	} else {
	    $.ajax({
	        type: 'POST',
	        dataType: 'JSON',
        	url: 'http://creatrixorganizer.com/sc/api/Admin/company/save_data/_',
	        // url: 'http://localhost/asiaholiday/api/Admin/company/save_data/_',
	        headers: {
	            'Content-Type': 'application/x-www-form-urlencoded'
	        },
	        data: $('#form_agent').serialize(),
	        success: function(response) {
	        	if (response.code == 200) {
	        		alert('success');
					$('#modal_add').modal('hide');
		        	get_data();
	        	} else {
	        		alert('failed, try again later');
					$('#modal_add').modal('hide');
		        	get_data();
	        	}
	        },
	    });
	}
}