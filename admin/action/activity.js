var format = 'dd-mm-yy',
from = $('#dateFrom')
.datepicker({
  dateFormat: 'dd-mm-yy',
  defaultDate: '+1w',
  numberOfMonths: 1
})
.on('change', function() {
  to.datepicker('option','minDate', getDate( this ) );
}),
to = $('#dateTo').datepicker({
  dateFormat: 'dd-mm-yy',
  defaultDate: '+1w',
  numberOfMonths: 1
})
.on('change', function() {
  from.datepicker('option','maxDate', getDate( this ) );
});

function getDate( element ) {
  var date;
  try {
    date = $.datepicker.parseDate( format, element.value );
  } catch( error ) {
    date = null;
  }

  return date;
}

$(function(){
  'use strict'

  var now   = new Date();
  var month = (now.getMonth()+1) < 10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1);

  $('#month').val(month);

  $('#optionDate').on('change',function(){
    if ($(this).val() == 0) {
      $('.monthly').css('display','block');
      $('.dateRange').css('display','none');
      $('#dateFrom').val('');
      $('#resetDateFrom').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
      $('#dateTo').val('');
      $('#resetDateTo').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    } else {
      $('.monthly').css('display','none');
      $('.dateRange').css('display','block');
      $('#dateFrom').val('');
      $('#resetDateFrom').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
      $('#dateTo').val('');
      $('#resetDateTo').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    }
  });

  $('#dateFrom, #dateTo, #month, #year').on('change', function() {
    get_data($('#staffID').val());
  }); 

  $('#dateFrom').on('change keyup paste', function (e) {
    if ($('#dateFrom').val() != '') {
      $('#resetDateFrom').attr('style','color:white;background-color:#dc3545;border:1px solid #dc3545');
    } else {
      $('#resetDateFrom').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    }
  });

  $('#resetDateFrom').click(function() {
    $('#dateFrom').val('');
    $('#resetDateFrom').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    get_data($('#staffID').val());
  });

  $('#dateTo').on('change keyup paste', function (e) {
    if ($('#dateTo').val() != '') {
      $('#resetDateTo').attr('style','color:white;background-color:#dc3545;border:1px solid #dc3545');
    } else {
      $('#resetDateTo').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    }
  });

  $('#resetDateTo').click(function() {
    $('#dateTo').val('');
    $('#resetDateTo').attr('style','color:#596882;background-color:#f5f6fa;border:1px solid #c0ccda');
    get_data($('#staffID').val());
  });

  get_staff();
  $('#staffID').on('change',function(){
    get_data($(this).val());
  });
})

function get_staff(){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'http://creatrixorganizer.com/sc/api/Admin/staff/get_data/_',
    // url: 'http://localhost/asiaholiday/api/admin/staff/get_data/_',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    data : {
      agentID : agentID,
    },
    success: function(responses) {
      $('#staffID').empty();
      if(responses.code == 200){
        $('#staffID').append(`
          <option value="0">ALL STAFF</option>
        `);
        get_data(0);
        $.each(responses.data, function(i, val){
          $('#staffID').append(`
            <option value="`+val.id+`">`+val.name+`</option>
          `);
        });
      }
    }
  });
}

function get_data(id){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: 'http://creatrixorganizer.com/sc/api/Admin/activity/get_data/_',
    // url: 'http://localhost/asiaholiday/api/admin/activity/get_data/_',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data :{
      id      : id,
      agentID : agentID,
      month   : $('#month').val(),
      year    : $('#year').val(),
      from    : $('#dateFrom').val(),
      to      : $('#dateTo').val(),
    },
    success: function(responses) {
      if(responses.code == 200){
        var aaData = [];
        $.each(responses.data, function( i, value ) {
          var stringData  = value._;
          var no          = i + 1;
          var lokasi      = value.location.split(',');
          var gambar1     = value.img1 == "" ? '' : `<a href="https://tghapisystem.net/api-creatrix/new/upload/` + value.img1 + `" target="_blank"><img src="https://tghapisystem.net/api-creatrix/new/upload/` + value.img1 + `" style="width:30%"></img></a>`;
          var gambar2     = value.img2 == "" ? '' : `<a href="https://tghapisystem.net/api-creatrix/new/upload/` + value.img2 + `" target="_blank"><img src="https://tghapisystem.net/api-creatrix/new/upload/` + value.img2 + `" style="width:30%;margin-left:10px"></img></a>`;
          var gambar3     = value.img3 == "" ? '' : `<a href="https://tghapisystem.net/api-creatrix/new/upload/` + value.img3 + `" target="_blank"><img src="https://tghapisystem.net/api-creatrix/new/upload/` + value.img3 + `" style="width:30%;margin-left:10px"></img></a>`;

          aaData.push([
            `<div style="text-align:center">` + no + `</div>`,
            dateFormat(value.uploadDate),
            hourFormat(value.uploadDate),
            value.name,
            lokasi[0]+`, `+lokasi[1] +`<a href="javascript:;" class="tx-12 tx-medium" onclick="detail_lokasi('`+value.location+`',`+value.latitude+`,`+value.longitude+`)"> View Map<i class="fas fa-arrow-circle-right mg-l-5"></i></a>`,
            value.caption,
            `<div style="text-align:center">`+gambar1+``+gambar2+``+gambar3+`</div>`,
          ]);
        });
        $('#main-table').DataTable().destroy();
        $('#main-table').DataTable({
        // 'responsive': true,
          'language'  : {
            'searchPlaceholder'   : 'Search...',
            'sSearch'       : '',
            'lengthMenu'      : '_MENU_ items/page',
            },
          'aaData'  : aaData,
          "bJQueryUI" : true,
          "aoColumns" : [
            { "sWidth"  : "1%" },  
            { "sWidth"  : "5%" }, 
            { "sWidth"  : "1%" }, 
            { "sWidth"  : "1%" }, 
            { "sWidth"  : "9%" }, 
            { "sWidth"  : "7%" }, 
            { "sWidth"  : "8%" },
          ],
          'aLengthMenu' : [
            [5, 10, 50, -1],
            [5, 10, 50, "All"]
          ],
        });
      }
    }
  });
}

function detail_lokasi(lokasi,latitude,longitude){
  $('#detail_lokasi').modal('show');
  $('#alamat').html(lokasi);
  $('#lokasi').html(`<iframe src="https://maps.google.com/maps?q=`+latitude+`,`+longitude+`&hl=en&z=16&amp;'+'&output=embed" width="100%"></iframe>`);
}