<?php include "header.php"; ?>

<div class="content content-fixed">
  <div class="container pd-x-0 pd-lg-x-0 pd-xl-x-0" style="max-width: none">
    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-10">
      <div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="home">Dashboard / Staff Monitoring</a></li>
          </ol>
          <h4 class="mg-b-0 tx-spacing--1"><?php echo date('d F Y') ?></h4>
        </nav>
      </div>
      <div class="d-none d-md-block">
      </div>
    </div>
    <div class="row row-xs" id="konten">
    </div>
  </div>
</div>

<?php include "footer.php"; ?>
<script src="../action/wholejs.js" type="text/javascript"></script>
<script src="../action/dashboard.js" type="text/javascript"></script>

<style type="text/css">
  .tabelGift tr td{
    padding: 5px !important;
    line-height: normal !important;
  }
  .card-body {
    padding: 10px;
  }
</style>