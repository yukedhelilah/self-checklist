<?php include "header.php"; ?>

<div class="content content-fixed">
  <div class="container pd-x-0 pd-lg-x-0 pd-xl-x-0" style="max-width: none">
    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-10">
      <div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="home">Dashboard / Activity</a></li>
          </ol>
        </nav>
      </div>
      <div class="d-none d-md-block">
      </div>
    </div>


    <fieldset class="form-fieldset">
      <legend>FILTER</legend>
        <div class="form-group row">
          <label for="projectID" class="col-sm-1 col-form-label">Staff</label>
          <div class="col-sm-3">
            <select class="custom-select" id="staffID" required style="width: 100%" /></select>
          </div>
          <label for="optionDate" class="col-sm-1 col-form-label">Option</label>
          <div class="col-sm-3">
            <select class="custom-select" id="optionDate" required style="width: 100%"/>
              <option value="0">MONTHLY</option>
              <option value="1">DATE RANGE</option>
            </select>
          </div>
          <div class="col-sm-2 monthly">
            <select class="custom-select" id="month" required style="width: 100%"/>
                <?php
                for ($i=1; $i<=12; $i++){
                    $j = $i<10 ? '0'.$i : $i;
                    $tgl = $i<10 ? date('Y').'-0'.$i.'-01' : date('Y').'-'.$i.'-01';
                    $date_str = date('F', strtotime($tgl));
                    echo "<option value='".$j."'>".$date_str ."</option>";
                } ?>
            </select>
          </div>
          <div class="col-sm-2 monthly">
            <select class="custom-select" id="year" required style="width: 100%"/>
                <?php
                for ($i=date('Y'); $i>=2020; $i--){
                    echo "<option value='".$i."'>".$i ."</option>";
                } ?>
            </select>
          </div>
          <div class="col-sm-2 dateRange" style="display: none">
            <div class="input-group">
              <input type="text" class="form-control" id="dateFrom" placeholder="From" autocomplete="off">
              <div class="input-group-append">
                <span class="input-group-text" id="resetDateFrom"><i class="fa fa-times"></i></span>
              </div>
            </div>
          </div>
          <div class="col-sm-2 dateRange" style="display: none">
            <div class="input-group">
              <input type="text" class="form-control" id="dateTo" placeholder="To" autocomplete="off">
              <div class="input-group-append">
                <span class="input-group-text" id="resetDateTo"><i class="fa fa-times"></i></span>
              </div>
            </div>
          </div>
        </div>
    </fieldset>

    <div class="row row-xs mg-t-10">
      <div class="col-lg-12 col-xl-12 mg-t-12">
          <table id="main-table" class="table" style="width: 100%">
            <thead>
              <th style="text-align: center;">NO</th>
              <th>TANGGAL</th>
              <th>JAM</th>
              <th>STAFF</th>
              <th>LOKASI</th>
              <th>AKTIFITAS</th>
              <th style="text-align: center;">GAMBAR</th>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="detail_lokasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="max-width: 500px">
    <div class="modal-content tx-14">
      <div class="modal-header pd-x-15 pd-y-10">
        <h6 class="modal-title" id="alamat"></h6>
      </div>
      <div class="modal-body" id="lokasi">
      </div>
      <div class="modal-footer pd-10">
        <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php include "footer.php"; ?>
<script src="../action/wholejs.js" type="text/javascript"></script>
<script src="../action/activity.js" type="text/javascript"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2OxOvDw6Qzm-LW8nkWeqmCgW-Rqd-hn0&callback=initMap" type="text/javascript"></script>

<style type="text/css">
  select[name="main-table_length"] {
    border: 1px solid rgb(226, 229, 237);
    padding: 7px 10px;
    border-radius: 5px; 
  }
</style>