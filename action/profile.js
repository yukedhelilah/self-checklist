$(document).ready(function(){
  var limit   = 7;
  var start   = 0;
  var action  = 'inactive';

  function get_content(limit, start){
    $.ajax({
      method:"POST",
      dataType: 'JSON',
      // url: 'https://arthabanguncemerlang.com/ashol-checklist-activity/staff/content/get_my_content/_',
      url:'http://creatrixorganizer.com/sc/api/staff/content/get_my_content/_',
      data:{
        start   : start,
        limit   : limit,
        staffID : staffID,
      },
      cache:false,
      success:function(responses){
        $.each(responses.data, function( i, val ) {
            var delete_content;
            if (val.uploadBy == staffID) {
                delete_content = `<i onclick="delete_content(`+val.id+`)" class="fa fa-trash color-red2-dark" style="float: right;position: inherit;margin-top: 8px;margin-right: 5px;"></i>`;
            } else {
                delete_content = `<i class="fa fa-trash color-red2-dark" style="float: right;position: inherit;margin-top: 8px;margin-right: 5px;display:none"></i>`;
            }

            var content = `
                <div class="content-boxed">
                    <div class="content bottom-0">
                        <div class="review-6 container">`+delete_content+`
                            <h1 style="font-size: 15px">`+val.name+`</h1>
                            <span class="color-theme" style="font-size: 9px">
                                `+ dateFormat(val.uploadDate) +` - <i class="fa fa-map-marker-alt color-blue2-dark" style="min-width:1px"></i> `+val.location+`
                            </span>
                            <div class="divider bottom-0 top-5" style="background-color: #d4d4d4;"></div>
                            <p class="top-10 bottom-0">`+val.caption+`</p>
                            <div class="gallery gallery-thumbs gallery-square bottom-20" style="padding-left: 6px">  
            `;

            if (val.img1 !== "") {
                content += `
                    <a class="default-link top-10" href="gallery_detail?search=`+btoa('home|'+val.img1)+`">
                        <img src="http://creatrixorganizer.com/sc/api/files/upload/`+val.img1+`" data-src="http://creatrixorganizer.com/sc/api/files/upload/`+val.img1+`" class="preload-image responsive-image" alt="img">
                    </a>        
                `;
            }

            if (val.img2 !== "") {
                content += `
                    <a class="default-link top-10" href="gallery_detail?search=`+btoa('home|'+val.img2)+`">
                        <img src="http://creatrixorganizer.com/sc/api/files/upload/`+val.img2+`" data-src="http://creatrixorganizer.com/sc/api/files/upload/`+val.img1+`" class="preload-image responsive-image" alt="img">
                    </a>        
                `;
            }

            if (val.img3 !== "") {
                content += `
                    <a class="default-link top-10" href="gallery_detail?search=`+btoa('home|'+val.img3)+`">
                        <img src="http://creatrixorganizer.com/sc/api/files/upload/`+val.img3+`" data-src="http://creatrixorganizer.com/sc/api/files/upload/`+val.img1+`" class="preload-image responsive-image" alt="img">
                    </a>        
                `;
            }


            content += `
                            </div>
                        </div>
                    </div>
                </div>
            `;

            $('#posting_content').append(content);
        });

        if(responses.data == null){
          $('#load_data_message').html(`
            <div style="width:100%;padding:10px;text-align:center;font-size:10px;font-weight:bold;color:#353535"> 2020 © Creatrix Organizer</div>
          `);
          action = 'active';
        }else{
          $('#load_data_message').html(`
            <div class="demo-preloader" style="width:100%;">
              <div class="preload-spinner border-teal-dark"></div>
            </div>
          `);
          action = "inactive";
        }
      }
    });
  }

  if(action == 'inactive'){
    action = 'active';
    get_content(limit, start);
  }

  $(window).scroll(function(){
    if($(window).scrollTop() + $(window).height() > $("#posting_content").height() && action == 'inactive'){
      action = 'active';
      start = start + limit;
      setTimeout(function(){
        get_content(limit, start);
      }, 1000);
    }
  });
});

function post_content(){      
  if ($('#caption').val() == "") {
    alert("Tuliskan sesuatu");
  } else {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      // url: 'https://arthabanguncemerlang.com/ashol-checklist-activity/staff/content/post_content/_',
      url: 'http://creatrixorganizer.com/sc/api/staff/content/post_content/_',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: $("#form_content").serialize()+"&staffID="+staffID,
      success: function(response) {
        if (response.code == '200') {
          alert('success');
          profile();
        }else{
          alert("failed");
        }
      },
    });
  }
}

function delete_content(id){
    if (confirm("Anda yakin menghapus postingan ini?")) {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            // url: 'https://arthabanguncemerlang.com/ashol-checklist-activity/staff/content/delete_content/_',
            url: 'http://creatrixorganizer.com/sc/api/staff/content/delete_content/_',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: {
                'token'  : '1b787b70ed2e0de697d731f14b5da57b',
                'id'     : id
            },
            success: function (responses) {
                if (responses.code == 200) {
                    home();
                }
            }
        });
    }
}