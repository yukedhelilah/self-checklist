var staffID     = atob(localStorage.getItem(btoa("staff_id")));
var name        = atob(localStorage.getItem(btoa("staff_name")));
var password    = atob(localStorage.getItem(btoa("staff_password")));
var expired     = localStorage.getItem(btoa("staff_expired"));
var exp         = new Date(atob(expired));
var now         = new Date();
var country; var region; var city;

if(staffID == null || name == null || expired == null || exp.getDate() +`-`+ exp.getMonth() +`-`+ exp.getFullYear() != now.getDate() +`-`+ now.getMonth() +`-`+ now.getFullYear()){
  removeLocalstorage();
  window.location.href = '../pages/login';
}

var monthNames  = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

var weekday     = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

function dateFormat(d) {
  var t       = new Date(d);
  var now     = new Date();
  var hours   = t.getHours() < 10 ? '0'+t.getHours() : t.getHours();
  var minutes = t.getMinutes() < 10 ? '0'+t.getMinutes() : t.getMinutes(); 

  if (t.getDate()+''+t.getMonth()+''+t.getFullYear() == now.getDate()+''+now.getMonth()+''+now.getFullYear()) {
    return "Today at " + hours + ':' + minutes;
  } else if (t.getDate()+''+t.getMonth()+''+t.getFullYear() == (now.getDate()-1)+''+now.getMonth()+''+now.getFullYear()) {
    return "Yesterday at " + hours + ':' + minutes;
  } else {
    return weekday[t.getDay()] + ', ' +t.getDate() + ' ' + monthNames[t.getMonth()] + ' ' + t.getFullYear() + ' at ' + hours + ':' + minutes;
  }
}

$(document).ready(function() {
  $("#upload1").change(function () {
    uploadFile(1);
  });

  $("#upload2").change(function () {
    uploadFile(2);
  });

  $("#upload3").change(function () {
    uploadFile(3);
  });

  $('#del_img1').click(function(){
    $("#div_upload1").css("display","block");
    $("#div_photo1").css("display","none");
    $('#image1').val('');
  });

  $('#del_img2').click(function(){
    $("#div_upload2").css("display","block");
    $("#div_photo2").css("display","none");
    $('#image2').val('');
  });

  $('#del_img3').click(function(){
    $("#div_upload3").css("display","block");
    $("#div_photo3").css("display","none");
    $('#image3').val('');
  });
});

$.get("https://api.ipdata.co?api-key=test", function (response) {
  $("#response").html(JSON.stringify(response, null, 4));
  $('#country').val(response.country_name);
  $('#region').val(response.region);
  $('#city').val(response.city);  
  $('#latitude').val(response.latitude);
  $('#longitude').val(response.longitude);
}, "jsonp");

function upload(no){
  $("#upload"+no).get(0).click();
}

function uploadFile(no){
  if (($("#upload"+no))[0].files.length > 0) {
      var file = $("#upload"+no)[0].files[0];
    var formdata = new FormData();
    formdata.append("userfile", file);
    formdata.append("no", no);
    var ajax = new XMLHttpRequest();
    ajax.addEventListener("load", completeHandler, false);
    ajax.open("POST", "http://creatrixorganizer.com/sc/api/staff/content/upload/_");
    // ajax.open("POST", "http://localhost/asiaholiday/api/staff/content/upload/_");
    ajax.send(formdata);
  } else {
      alert("No file chosen!");
  }
}

function completeHandler(event){
  var data = event.target.responseText.split('*');
  var no = data[2];
  if(data[0]!=''){
    $('#upload'+no).val('');
    alert("Error!"+ data[1]);
  }else{
    $('#image'+no).val(data[1]);
    loadImg();
    $('#upload'+no).val('');
  } 
}

function loadImg(){
  for (var no = 1; no <= 3; no++) {
    if($('#image'+no).val() == ''){
      $("#div_upload"+no).css("display","block");
      $("#div_photo"+no).css("display","none");
    }else{
      $("#div_upload"+no).css("display","none");
      $("#div_photo"+no).css("display","block");
      $("#img"+no).attr("src", "http://localhost/asiaholiday/api/files/upload/"+$('#image'+no).val());
      $("#img"+no).attr("data-src", "http://localhost/asiaholiday/api/files/upload/"+$('#image'+no).val());
      // $("#div_photo"+no).attr("href","gallery_detail?search="+btoa($('#image'+no).val()));
    }
  }
}

function getUrlParameter(sParam){
  var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
  }
}

function home(){
  window.location.href = '../pages/home';
}

function profile(){
  window.location.href = '../pages/profile';
}

function logout() {
  if (confirm("Do you want to logout?")) {
    removeLocalstorage();
    window.location.href = '../pages/login';
  } 
}

function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
    return true;
}

function removeLocalstorage(){
  localStorage.removeItem(btoa('staff_id'));
  localStorage.removeItem(btoa('staff_name'));
  localStorage.removeItem(btoa('staff_password'));
  localStorage.removeItem(btoa('staff_token'));
  localStorage.removeItem(btoa('staff_expired'));
}