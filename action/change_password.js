$('#password').focus();

$("#form_change_password").submit(function(e){  
    e.preventDefault()
    change_password();       
});

function change_password(){
    if ($('#password').val() == '') {
        alert('Password harus diisi');
    } else if ($('#password').val() == '123456') {
        alert('Password tidak boleh 123456');
    }  else {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            // url: 'https://arthabanguncemerlang.com/ashol-checklist-activity/staff/login/change_password/_',
            url: 'http://creatrixorganizer.com/sc/api/staff/login/change_password/_',
            headers: {
                'Content-Type'  : 'application/x-www-form-urlencoded',
            },
            data: {
                'password' : $('#password').val(),
                'staffID'   : atob(localStorage.getItem(btoa("staff_id"))),
            },
            success: function(response) {
                if (response.code == '200') {
                    window.location.href = '../pages/login';
                    localStorage.removeItem(btoa('staff_id'));
                    localStorage.removeItem(btoa('staff_name'));
                    localStorage.removeItem(btoa('staff_password'));
                    localStorage.removeItem(btoa('staff_token'));
                    localStorage.removeItem(btoa('staff_expired'));
                }else{
                    alert("failed");
                }
            },
        });
    }
}