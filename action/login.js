$('#form_signin').trigger('reset');
$('input[name="username"]').focus();

$(document).ready(function() { 
    if (localStorage.getItem(btoa('staff_expired')) != null && localStorage.getItem(btoa('staff_id')) != null && localStorage.getItem(btoa('staff_name')) != null) {
        if (atob(localStorage.getItem(btoa("staff_password"))) == '1') {
            window.location.href = '../pages/home';
        } else {
            window.location.href = '../pages/change_password';
        }
    }
});

$("#form_signin").submit(function(e){  
    e.preventDefault()  
    login();
});

function login(){     
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: 'http://creatrixorganizer.com/sc/api/staff/login/login/_',
        // url: 'http://localhost/asiaholiday/api/staff/login/login/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $("#form_signin").serialize()+"&token="+'1b787b70ed2e0de697d731f14b5da57b',
        success: function(response) {
            if (response.code == '200') {
                if (response.data.password == 0) {
                    window.location.href = '../pages/change_password';
                } 
                else {
                    window.location.href = '../pages/home';
                }
                localStorage.setItem(btoa('staff_id'), btoa(response.data.id));
                localStorage.setItem(btoa('staff_name'), btoa(response.data.name));
                localStorage.setItem(btoa('staff_password'), btoa(response.data.password));
                localStorage.setItem(btoa('staff_token'), btoa(response.data.token));
                localStorage.setItem(btoa('staff_expired'), btoa(response.data.expired));
            }else{
                alert("Your account is not registered");
            }
        },
    });
}